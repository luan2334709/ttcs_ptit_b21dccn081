import React from "react";
import { Routes, Route } from "react-router-dom";
import Hero from "./components/Hero";
import Home from "./components/Home";
import DetailMovies from "./components/DetailMovies";
import Movies from "./components/Movies";
import Series from "./components/Series";
import DetailSeries from "./components/DetailSeries";
import WatchPart from "./components/WathPart";
function App() {
    return (
        <>
            <Routes>
                <Route path="/" element={<Hero />} />
                <Route path="/home" element={<Home />} />
                <Route path="/movies" element={<Movies />} />
                <Route path="/series" element={<Series />} />
                <Route path="/movies/detail/:id" element={<DetailMovies />} />
                <Route path="/series/detail/:id" element={<DetailSeries />} />
                <Route
                    path="/series/detail/:id/:part"
                    element={<WatchPart />}
                />
            </Routes>
        </>
    );
}

export default App;
