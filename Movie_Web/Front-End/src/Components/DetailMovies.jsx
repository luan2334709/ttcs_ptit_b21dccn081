import React, { useState, useEffect } from 'react';
import axios from 'axios';
import DefaultLayout from './Layouts/default';
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";

const DetailMovies = () => {
  const { id } = useParams();
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [actionData, setActionData] = useState([]);
  const [romanData, setRomanData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      setError(null);

      try {
        const response = await axios.get(`http://localhost:3000/movies/detail/${id}`);
        setData(response.data);
      } catch (error) {
        console.error('Failed to fetch movie data:', error);
        setError(error);
      } finally {
        setIsLoading(false);
      }
    };


    fetchData();
  }, [id]);
  useEffect(() => {
    const fetchData = async () => {
      setError(null);

      try {
        const response = await axios.get(
          "http://localhost:3000/movies/action"
        );
        setActionData(response.data);
      } catch (error) {
        console.error("Failed to fetch movie data:", error);
        setError(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, []);
  useEffect(() => {
    const fetchData = async () => {
        setError(null);
        try {
            const response = await axios.get(
                "http://localhost:3000/movies/romantic"
            );
            setRomanData(response.data);
        } catch (error) {
            console.error("Failed to fetch movie data:", error);
            setError(error);
        } finally {
            setIsLoading(false);
        }
    };

    fetchData();
}, []);

  if (isLoading) return <div className='text-white'>Loading movie...</div>;
  if (error) return <div className='text-white'>Error: {error.message}</div>;

  return (
    <DefaultLayout>
      {data && (
        <div className='text-white mx-8'>
          <div>
            <iframe src={data.VideoLink }  title="This is a unique title" width="100%" height="650" allowFullScreen allow='autoplay'></iframe>
          </div>
          <div className="flex gap-12 my-9">
            <div >
              <img
                className="rounded-lg shadow-lg h-60 w-60 object-contain"
                src={data.ImageSrc}
                alt={data.engName}
              />
            </div>
            <div>
              <h2 className='font-bold text-3xl'>{data.Name}</h2>
              <p className='font-thin text-lg my-2'>Name: {data.engName}</p>
              <p className='font-thin text-lg my-2'>Quốc gia: {data.Country}</p>
              <p className='font-thin text-lg my-2'>Thời lượng: {data.Runtime}</p>
              <p className='font-thin text-lg my-2'>Mô tả: {data.Description}</p>
            </div>
          </div>
        </div>
      )}
      <p className="text-white text-center font-bold text-3xl my-8">
        Action Movies
      </p>
      <Swiper
        spaceBetween={3}
        slidesPerView={6}
        onSlideChange={() => console.log("slide change")}
        onSwiper={(swiper) => console.log(swiper)}
      >
        {actionData &&
          actionData.map((item, index) => {
            return (
              <SwiperSlide key={index}>
                <Link to={`/movies/detail/${item._id}`}>
                  <div className="w-50">
                    <img
                      src={item.ImageSrc}
                      alt={item.Name}
                      className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"
                    />
                    <p className="text-white w-50 my-3">
                      {item.Name}
                    </p>
                  </div>
                </Link>
              </SwiperSlide>
            );
          })}
      </Swiper>
      <p className="text-white text-center font-bold text-3xl my-8">
        Romantic Movies
      </p>
      <Swiper
        spaceBetween={3}
        slidesPerView={6}
        onSlideChange={() => console.log("slide change")}
        onSwiper={(swiper) => console.log(swiper)}
      >
        {romanData &&
          romanData.map((item, index) => {
            return (
              <SwiperSlide key={index}>
                <Link to={`/movies/detail/${item._id}`}>
                  <div className="w-50">
                    <img
                      src={item.ImageSrc}
                      alt={item.Name}
                      className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"
                    />
                    <p className="text-white w-50 my-3">
                      {item.Name}
                    </p>
                  </div>
                </Link>
              </SwiperSlide>
            );
          })}
      </Swiper>
    </DefaultLayout>
  );
}

export default DetailMovies;
