import React, { useEffect, useState } from "react";
import axios from "axios";
import DeFaultLayout from "./Layouts/default";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { Link } from "react-router-dom";

const Series = () => {
    const [searchTerm, setSearchTerm] = useState("");
    const [searchData, setSearchData] = useState([]);

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (searchTerm.trim() !== "") {
            try {
                const response = await axios.post(
                    "http://localhost:3000/series/search",
                    {
                        term: searchTerm,
                    },
                    {
                        headers: {
                            "Content-Type": "application/json",
                        },
                    }
                );
                setSearchData(response.data);
            } catch (error) {
                console.error("Failed to post search term:", error);
            }
        }
    };
    const onKeyDown = (e) => {
        if (e.target.value !== "") {
            setTimeout(() => {
                if (e.key === "Enter") {
                    setSearchTerm("");
                }
            }, 50);
        }
    };
    const [actionData, setActionData] = useState([]);
    const [romanData, setRomanData] = useState([]);
    const [hornorData, sethornorData] = useState([]);
    const [cartoonData, setCartoonData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            setError(null);

            try {
                const response = await axios.get(
                    "http://localhost:3000/series/action"
                );
                setActionData(response.data);
            } catch (error) {
                console.error("Failed to fetch movie data:", error);
                setError(error);
            } finally {
                setIsLoading(false);
            }
        };

        fetchData();
    }, []);
    useEffect(() => {
        const fetchData = async () => {
            setError(null);
            try {
                const response = await axios.get(
                    "http://localhost:3000/series/romantic"
                );
                setRomanData(response.data);
            } catch (error) {
                console.error("Failed to fetch movie data:", error);
                setError(error);
            } finally {
                setIsLoading(false);
            }
        };

        fetchData();
    }, []);
    useEffect(() => {
        const fetchData = async () => {
            setError(null);
            try {
                const response = await axios.get(
                    "http://localhost:3000/series/hornor"
                );
                sethornorData(response.data);
            } catch (error) {
                console.error("Failed to fetch movie data:", error);
                setError(error);
            } finally {
                setIsLoading(false);
            }
        };

        fetchData();
    }, []);
    useEffect(() => {
        const fetchData = async () => {
            setError(null);
            try {
                const response = await axios.get(
                    "http://localhost:3000/series/cartoon"
                );
                setCartoonData(response.data);
            } catch (error) {
                console.error("Failed to fetch movie data:", error);
                setError(error);
            } finally {
                setIsLoading(false);
            }
        };

        fetchData();
    }, []);
    return (
        <DeFaultLayout>
            <p className="text-white text-center font-bold text-3xl my-8">
                Enjoy your freetime with my website !
            </p>
            <div className="flex justify-center my-12">
                <div>
                    <form onSubmit={handleSubmit}>
                        <input
                            className="border w-90 border-gray-300 p-2 rounded-lg w-full focus:outline-none focus:ring-2 focus:ring-gray-200 focus:border-transparent"
                            type="text"
                            value={searchTerm}
                            onChange={(event) =>
                                setSearchTerm(event.target.value)
                            }
                            placeholder="Search for movies..."
                            onKeyDown={onKeyDown}
                        />
                    </form>
                </div>
            </div>
            {searchData && searchData.length > 0 && (
                <p className="text-white text-center font-bold text-3xl my-8">
                    Search for movies
                </p>
            )}
            <Swiper
                spaceBetween={3}
                slidesPerView={6}
                onSlideChange={() => console.log("slide change")}
                onSwiper={(swiper) => console.log(swiper)}
            >
                {searchData &&
                    searchData.map((item, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <Link to={`/series/detail/${item._id}`}>
                                    <div className="w-50">
                                        <img
                                            src={item.ImageSrc}
                                            alt={item.title}
                                            className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"
                                        />
                                        <p className="text-white w-50 my-3">
                                            {item.title}
                                        </p>
                                    </div>
                                </Link>
                            </SwiperSlide>
                        );
                    })}
                <p className="text-white text-center font-bold text-3xl my-8">
                    Action Series
                </p>
            </Swiper>
            <Swiper
                spaceBetween={3}
                slidesPerView={6}
                onSlideChange={() => console.log("slide change")}
                onSwiper={(swiper) => console.log(swiper)}
            >
                {actionData &&
                    actionData.map((item, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <Link to={`/series/detail/${item._id}`}>
                                    <div className="w-50">
                                        <img
                                            src={item.ImageSrc}
                                            alt={item.title}
                                            className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"
                                        />
                                        <p className="text-white w-50 my-3">
                                            {item.title}
                                        </p>
                                    </div>
                                </Link>
                            </SwiperSlide>
                        );
                    })}
            </Swiper>
            <p className="text-white text-center font-bold text-3xl my-8">
                Romantic Series
            </p>
            <Swiper
                spaceBetween={3}
                slidesPerView={6}
                onSlideChange={() => console.log("slide change")}
                onSwiper={(swiper) => console.log(swiper)}
            >
                {romanData &&
                    romanData.map((item, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <Link to={`/series/detail/${item._id}`}>
                                    <div className="w-50">
                                        <img
                                            src={item.ImageSrc}
                                            alt={item.title}
                                            className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"
                                        />
                                        <p className="text-white w-50 my-3">
                                            {item.title}
                                        </p>
                                    </div>
                                </Link>
                            </SwiperSlide>
                        );
                    })}
            </Swiper>
            <p className="text-white text-center font-bold text-3xl my-8">
                Hornor Series
            </p>
            <Swiper
                spaceBetween={3}
                slidesPerView={6}
                onSlideChange={() => console.log("slide change")}
                onSwiper={(swiper) => console.log(swiper)}
            >
                {hornorData &&
                    hornorData.map((item, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <Link to={`/series/detail/${item._id}`}>
                                    <div className="w-50">
                                        <img
                                            src={item.ImageSrc}
                                            alt={item.title}
                                            className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"
                                        />
                                        <p className="text-white w-50 my-3">
                                            {item.title}
                                        </p>
                                    </div>
                                </Link>
                            </SwiperSlide>
                        );
                    })}
            </Swiper>
            <p className="text-white text-center font-bold text-3xl my-8">
                Cartoon Movies
            </p>
            <Swiper
                spaceBetween={3}
                slidesPerView={6}
                onSlideChange={() => console.log("slide change")}
                onSwiper={(swiper) => console.log(swiper)}
            >
                {actionData &&
                    actionData.map((item, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <Link to={`/movies/detail/${item._id}`}>
                                    <div className="w-50">
                                        <img
                                            src={item.ImageSrc}
                                            alt={item.title}
                                            className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"
                                        />
                                        <p className="text-white w-50 my-3">
                                            {item.title}
                                        </p>
                                    </div>
                                </Link>
                            </SwiperSlide>
                        );
                    })}
            </Swiper>
        </DeFaultLayout>
    );
};

export default Series;
