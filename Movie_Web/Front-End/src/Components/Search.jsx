import React, { useState } from "react";
import axios from "axios";

const Search = () => {
    const [searchTerm, setSearchTerm] = useState("");

    const handleSubmit = async (event) => {
        event.preventDefault();

        try {
            const response = await axios.post(
                "http://localhost:3000/movies/search",
                {
                    term: searchTerm,
                },
                {
                    headers: {
                        "Content-Type": "application/json",
                    },
                }
            );
            console.log(response.data);
        } catch (error) {
            console.error("Failed to post search term:", error);
        }
    };
    const onKeyDown = (e) => {
        setTimeout(() => {
            if (e.key === "Enter") {
                setSearchTerm("");
            }
        }, 50);
    };

    return (
        <form onSubmit={handleSubmit}>
            <input
                className="border w-90 border-gray-300 p-2 rounded-lg w-full focus:outline-none focus:ring-2 focus:ring-gray-200 focus:border-transparent"
                type="text"
                value={searchTerm}
                onChange={(event) => setSearchTerm(event.target.value)}
                placeholder="Search for movies..."
                onKeyDown={onKeyDown}
            />
        </form>
    );
};

export default Search;
