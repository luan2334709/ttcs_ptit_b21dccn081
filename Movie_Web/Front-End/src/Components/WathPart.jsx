import { useParams } from "react-router-dom";
import DefaultLayout from "./Layouts/default";
import { React, useEffect, useState } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
const WatchPart = () => {
    const { id, part } = useParams();
    console.log(id, part);
    const [data, setData] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);
    const [romanData, setRomanData] = useState([]);
    const [cartoonData, setCartoonData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            setError(null);
            try {
                const response = await axios.get(
                    "http://localhost:3000/series/romantic"
                );
                setRomanData(response.data);
            } catch (error) {
                console.error("Failed to fetch movie data:", error);
                setError(error);
            } finally {
                setIsLoading(false);
            }
        };

        fetchData();
    }, []);
    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            setError(null);

            try {
                const response = await axios.get(
                    `http://localhost:3000/series/detail/${id}`
                );
                setData(response.data);
            } catch (error) {
                console.error("Failed to fetch movie data:", error);
                setError(error);
            } finally {
                setIsLoading(false);
            }
        };

        fetchData();
    }, [id]);
    useEffect(() => {
        const fetchData = async () => {
            setError(null);
            try {
                const response = await axios.get(
                    "http://localhost:3000/series/cartoon"
                );
                setCartoonData(response.data);
            } catch (error) {
                console.error("Failed to fetch movie data:", error);
                setError(error);
            } finally {
                setIsLoading(false);
            }
        };

        fetchData();
    }, []);

    if (isLoading) return <div className="text-white">Loading movie...</div>;
    if (error) return <div className="text-white">Error: {error.message}</div>;

    return (
        <DefaultLayout>
            {data && (
                <div className="text-white mx-8">
                    <div>
                        <iframe
                            src={data.video_links[part - 1]}
                            width="100%"
                            height="650"
                            allowfullscreen
                            aria-controls
                            allow="fullscreen;autoplay"
                        ></iframe>
                    </div>
                    <div className="flex gap-12 my-9">
                        <div>
                            <img
                                className="rounded-lg shadow-lg h-60 w-60 object-contain"
                                src={data.ImageSrc}
                                alt={data.engName}
                            />
                        </div>
                        <div>
                            <h2 className="font-bold text-3xl">{data.title}</h2>
                            <p className="font-thin text-lg my-2">
                                Name: {data.engName}
                            </p>
                            <p className="font-thin text-lg my-2">
                                Trạng thái: {data.status}
                            </p>
                            <p className="font-thin text-lg my-2">
                                Ngày ra mắt: {data.time}
                            </p>
                            <p className="font-thin text-lg my-2">
                                Mô tả: {data.des}
                            </p>
                            <p className="text-green-300 font-bold my-2">
                                Chọn tập :{" "}
                            </p>
                            <div>
                                {data.video_links.map((item, idx) => {
                                    if (part - 1 === idx) {
                                        return (
                                            <Link
                                                to={`/series/detail/${id}/${
                                                    idx + 1
                                                }`}
                                            >
                                                <button
                                                    key={idx}
                                                    className="text-white bg-gradient-to-r from-blue-500 via-blue-600 to-blue-700 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-2.5 py-1.5 text-center me-2 mb-2 mx-1"
                                                >
                                                    {idx + 1}
                                                </button>
                                            </Link>
                                        );
                                    } else {
                                        return (
                                            <Link
                                                to={`/series/detail/${id}/${
                                                    idx + 1
                                                }`}
                                            >
                                                <button
                                                    key={idx}
                                                    className="text-white bg-gradient-to-r from-green-400 via-green-500 to-green-600 hover:bg-gradient-to-br focus:ring-4 focus:outline-none focus:ring-green-300 dark:focus:ring-green-800 font-medium rounded-lg text-sm px-2.5 py-1.5 text-center me-2 mb-2 mx-1"
                                                >
                                                    {idx + 1}
                                                </button>
                                            </Link>
                                        );
                                    }
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            )}
            <p className="text-white text-center font-bold text-3xl my-8">
                Relative Series
            </p>
            <Swiper
                spaceBetween={3}
                slidesPerView={6}
                onSlideChange={() => console.log("slide change")}
                onSwiper={(swiper) => console.log(swiper)}
            >
                {romanData &&
                    romanData.map((item, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <Link to={`/series/detail/${item._id}`}>
                                    <div className="w-50">
                                        <img
                                            src={item.ImageSrc}
                                            alt={item.title}
                                            className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"
                                        />
                                        <p className="text-white w-50 my-3">
                                            {item.title}
                                        </p>
                                    </div>
                                </Link>
                            </SwiperSlide>
                        );
                    })}
            </Swiper>
            <p className="text-white text-center font-bold text-3xl my-8">
                New Series
            </p>
            <Swiper
                spaceBetween={3}
                slidesPerView={6}
                onSlideChange={() => console.log("slide change")}
                onSwiper={(swiper) => console.log(swiper)}
            >
                {cartoonData &&
                    cartoonData.map((item, index) => {
                        return (
                            <SwiperSlide key={index}>
                                <Link to={`/series/detail/${item._id}`}>
                                    <div className="w-50">
                                        <img
                                            src={item.ImageSrc}
                                            alt={item.title}
                                            className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"
                                        />
                                        <p className="text-white w-50 my-3">
                                            {item.title}
                                        </p>
                                    </div>
                                </Link>
                            </SwiperSlide>
                        );
                    })}
            </Swiper>
        </DefaultLayout>
    );
};

export default WatchPart;
