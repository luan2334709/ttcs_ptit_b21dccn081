import React, { useEffect, useState } from "react";
import DeFaultLayout from './Layouts/default';
import { Navigation, Pagination, Scrollbar, A11y } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import { Link } from "react-router-dom";
import axios from 'axios';

const Home = () => {
    const homePoster = [
        {
            "_id": "660e390b39ba78d97a762bf6",
            "poster": "/images/heimer.jpeg",
            "ImageSrc": "https://image.tmdb.org/t/p/w185/hWRKHXKfu2WJGICdVpKZ0jEKsbp.jpg",
            "Name": "Einstein Và Trái Bom",
            "engName": "Einstein and the Bomb 2024",
            "VideoLink": "https://vip.opstream17.com/share/1ee3dfcd8a0645a25a35977997223d22",
            "Country": "United Kingdom",
            "Runtime": "76 phút",
            "Sgeneros": ["660e34e139ba78d97a703192", "660e386239ba78d97a75368a"],
            "Description": "Chuyện gì xảy ra sau khi Einstein trốn khỏi Đức Quốc xã? Qua phim tư liệu và lời chia sẻ của chính ông, bộ phim tài liệu chính kịch này đào sâu tâm trí của thiên tài bị dằn vặt."
        },
        {
            "_id": "660e390b39ba78d97a762bf0",
            "poster": "/images/upgrade.jpeg",
            "ImageSrc": "https://image.tmdb.org/t/p/w185/eYsdP5QffklSZMXJMy295xTHKmt.jpg",
            "Name": "Nâng Cấp",
            "engName": "Upgraded 2024",
            "VideoLink": "https://vip.opstream17.com/share/10c272d06794d3e5785d5e7c5356e9ff",
            "Country": "United Kingdom",
            "Runtime": "105 phút",
            "Sgeneros": ["660e34e139ba78d97a703192", "660e358d39ba78d97a71287e"],
            "Description": "Upgraded 2024 là một bộ phim hài lãng mạn đầy sáng tạo, đã được phát hành trên Amazon Prime Video vào ngày 9 tháng 2 năm 2024. Câu chuyện xoay quanh cuộc sống của Ana, một cô gái trẻ đầy tham vọng và mơ ước về một sự nghiệp trong lĩnh vực nghệ thuật. Trong quá trình cố gắng để gây ấn tượng với sếp khó tính của mình, Claire, Ana đã vô tình gặp Will, một chàng trai lịch lãm và quyến rũ. Một sự nhầm lẫn nhỏ đã khiến Ana trở thành sếp của mình. Với lời nói dối này, cuộc sống của cô bắt đầu thay đổi hoàn toàn. Ana và Will dần trở nên thân thiết và tình cảm giữa họ nảy nở. Tuy nhiên, những rắc rối và tình huống dở khóc dở cười không ngừng xảy ra khi Ana phải duy trì lời nói dối của mình để không bị phát hiện."
        },
        {
            "_id": "660e390b39ba78d97a762bf8",
            "poster": "/images/LightsOut.jpg",
            "ImageSrc": "https://image.tmdb.org/t/p/w185/usDIBB0hOqLM8wMSGistfqxjukZ.jpg",
            "Name": "Chiến Binh Ngầm",
            "engName": "Lights Out 2024",
            "VideoLink": "No link",
            "Country": "USA",
            "Runtime": "90 phút",
            "Sgeneros": ["660e35b739ba78d97a71668b", "660e355e39ba78d97a70e6c3"],
            "Description": "Một cựu quân nhân trôi dạt trở thành chiến binh ngầm với sự giúp đỡ của một cựu tù nhân vừa được thả, đẩy anh ta chống lại cảnh sát tham nhũng và những kẻ giết người được thuê hiện đang truy lùng anh ta và tất cả những người anh ta quan tâm."
        },
        
        {
            "_id": "660e390b39ba78d97a762bf3",
            "poster": "/images/madamweb.jpg",
            "ImageSrc": "https://image.tmdb.org/t/p/w185/gbz5wAan81Z2USmW8O1wMFfhkNO.jpg",
            "Name": "Quý Cô Mạng Nhện",
            "engName": "Madame Web 2024",
            "VideoLink": "https://vip.opstream13.com/share/8b3bac12926cc1d9fb5d68783376971d",
            "Country": "USA",
            "Runtime": "116 phút",
            "Sgeneros": ["660e346c39ba78d97a6f8a10", "660e36b439ba78d97a72d6c3"],
            "Description": "Bộ phim kể về nguồn gốc của một đồng minh Người Nhện, một nhân vật trong truyện tranh Marvel có sức mạnh liên kết đa vũ trụ – Cassandra Webb (Dakota Johnson) hay còn được biết đến là Madame Web. Sở hữu khả năng và giác quan nhạy bén của loài nhện, Cassandra có khả năng thấu thị, thần giao cách cảm và nhìn trước tương lai. Chính từ đây mà số phận của cô ấy trở nên gắn bó một cách bí ẩn với ba cô gái khác là Julia Carpenter (Sydney Sweeney), Anya Corazon (Isabela Merced) và Mattie Franklin (Celeste O’Connor) – những người được dự đoán là thế hệ tương lai của Người Nhện."
        },
    ]
    const [data, setData] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState(null);
    useEffect(() => {
        const fetchData = async () => {
          setIsLoading(true);
          setError(null); 
    
          try {
            const response = await axios.get(`http://localhost:3000/movies`);
            setData(response.data);
          } catch (error) {
            console.error('Failed to fetch movie data:', error);
            setError(error); 
          } finally {
            setIsLoading(false); 
          }
        };
    
        fetchData();
      }, []);
    
    const [actionData, setActionData] = useState(null);
    useEffect(() => {
        const fetchData = async () => {
        setIsLoading(true);
        setError(null); 
    
        try {
            const response = await axios.get(`http://localhost:3000/movies/action`);
            setActionData(response.data);
        } catch (error) {
            console.error('Failed to fetch movie data:', error);
            setError(error); 
        } finally {
            setIsLoading(false); 
        }
        };
        fetchData();
    })
    return (
        <DeFaultLayout>
            <div className="">
                <Swiper
                    className="cursor-pointer"
                    modules={[Navigation, Pagination, Scrollbar, A11y]}
                    spaceBetween={0}
                    slidesPerView={1}
                    onSwiper={(swiper) => console.log(swiper)}
                    onSlideChange={() => console.log('slide change')}
                    >
                    {
                        homePoster.map((item, index) => {
                            if(item.VideoLink !== "No Link"){
                                return (
                                    <SwiperSlide key={index}>
                                        <Link to={`/movies/detail/${item._id}`}>
                                            <div className="relative h-screen w-screen overflow-hidden">
                                                <img src={item.poster} alt={item.Name} className="absolute top-0 left-0 min-w-full min-h-full object-cover"/>
                                                <div className="absolute bottom-0 left-0 w-full flex flex-col items-center justify-center text-white p-4">
                                                    <h2 className="text-4xl mb-2">{item.Name}</h2>
                                                    <p className="mb-4">{item.Description}</p>
                                                    <button className="px-4 py-2 bg-red-500 text-2xl text-white rounded">Xem phim</button>
                                                </div>
                                            </div>
                                        </Link>
                                    </SwiperSlide>
                                )
                            }
                })
                    }
                </Swiper>
            </div>
                <div className = "my-10 select-none">
                    <h1 className = "text-white text-center text-3xl font-bold my-10">Phim Nổi Bật</h1>
                    <Swiper
                        spaceBetween={3}
                        slidesPerView={6}
                        onSlideChange={() => console.log('slide change')}
                        onSwiper={(swiper) => console.log(swiper)}
                        >
                        {data && data.map((item, index) => {
                            return (
                                <SwiperSlide key={index}>
                                    <Link to={`/movies/detail/${item._id}`}>
                                        <div className = "w-50">
                                            <img src={item.ImageSrc} alt={item.Name} className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"/>
                                            <p className ="text-white w-50 my-3">{item.Name}</p>
                                        </div>
                                    </Link>
                                </SwiperSlide>
                            )
                        })}
                    </Swiper>
                </div>
                <div className = "my-10 select-none">
                    <h1 className = "text-white text-center text-3xl font-bold my-10">Phim Hành Động</h1>
                    <Swiper
                        spaceBetween={3}
                        slidesPerView={6}
                        onSlideChange={() => console.log('slide change')}
                        onSwiper={(swiper) => console.log(swiper)}
                        >
                        {actionData && actionData.map((item, index) => {
                            return (
                                <SwiperSlide key={index}>
                                    <Link to={`/movies/detail/${item._id}`}>
                                        <div className = "w-50">
                                            <img src={item.ImageSrc} alt={item.Name} className="h-64 w-60 object-cover transform transition duration-500 hover:border-2"/>
                                            <p className ="text-white w-50 my-3">{item.Name}</p>
                                        </div>
                                    </Link>
                                </SwiperSlide>
                            )
                        })}
                    </Swiper>
                </div>
        </DeFaultLayout>
    );
}

export default Home;