import React from 'react';
import {
  FaFacebookSquare,
  FaGithubSquare,
  FaInstagram,
  FaTwitterSquare,
} from 'react-icons/fa';

const Footer = () => {
  return (
    <div className='max-w-[1240px] mx-auto py-16 px-4 grid lg:grid-cols-3 gap-8 text-gray-300'>
      <div>
        <h1 className='w-full text-3xl font-bold text-[#00df9a]'>Movies.</h1>
        <p className='py-4'>Create by Thanh Luan</p>
        <div className='flex justify-between md:w-[75%] my-6'>
            <a href="https://www.facebook.com/BoyBlack00" rel="noreferrer" target="_blank">
              <FaFacebookSquare size={30}/>
            </a>
            <a href="https://www.facebook.com/BoyBlack00" rel="noreferrer" target="_blank">
              <FaInstagram size={30} />
            </a>
            <a href="https://www.facebook.com/BoyBlack00" rel="noreferrer" target="_blank">
              <FaTwitterSquare size={30} />
            </a>
            <a href="https://www.facebook.com/BoyBlack00" rel="noreferrer" target="_blank">
              <FaGithubSquare size={30} />
            </a>
        </div>
      </div>
      <div className='lg:col-span-2 flex justify-between mt-6'>
    <div>
        <h6 className='font-medium text-gray-400'>Technique</h6>
        <ul>
            <li className='py-2 text-sm'>ReactJS</li>
            <li className='py-2 text-sm'>Express</li>
            <li className='py-2 text-sm'>TailwindCss</li>
            <li className='py-2 text-sm'>Boostrap 5</li>
        </ul>
    </div>
    <div>
        <h6 className='font-medium text-gray-400'>Support</h6>
        <ul>
            <li className='py-2 text-sm'>Watch</li>
            <li className='py-2 text-sm'>Search</li>
        </ul>
    </div>
    <div>
        <h6 className='font-medium text-gray-400'>Data</h6>
        <ul>
            <li className='py-2 text-sm'>MongoDB Atlas</li>
        </ul>
    </div>
    <div>
        <h6 className='font-medium text-gray-400'>Create by</h6>
        <ul>
            <li className='py-2 text-sm'>Thanh Luan</li>
            <li className='py-2 text-sm'>D21DCCN09-B</li>
            <li className='py-2 text-sm'>B21CQCN081</li>
        </ul>
    </div>
      </div>
    </div>
  );
};

export default Footer;
