import React, { useState } from 'react';
import { AiOutlineClose, AiOutlineMenu } from 'react-icons/ai';
import { Link } from 'react-router-dom';
const Navbar = () => {
  const [nav, setNav] = useState(false);

  const handleNav = () => {
    setNav(!nav);
  };

  return (
    <div className='flex justify-between items-center h-24 max-w-[1240px] mx-auto px-4 text-white select-none'>
      <h1 className='w-full text-3xl font-bold text-[#00df9a]'>
        <Link to="/">Movies.</Link>
      </h1>
      <ul className='hidden md:flex'>
        <li className='p-4 cursor-pointer py-1 hover:text-white gap-8 uppercase text-sm text-white font-medium relative after:absolute after:bottom-0 after:left-0 after:bg-teal-400 after:h-0.5 after:w-0 hover:after:w-full after:transition-all after:duration-500 after:ease-in-out mx-2'>
          <Link to='/home'>Home</Link>
        </li>
        <li className='p-4 cursor-pointer py-1 hover:text-white gap-8 uppercase text-sm text-white font-medium relative after:absolute after:bottom-0 after:left-0 after:bg-teal-400 after:h-0.5 after:w-0 hover:after:w-full after:transition-all after:duration-500 after:ease-in-out mx-2'>
          <Link to='/movies'>Movies</Link>
        </li>
        <li className='p-4 cursor-pointer py-1 hover:text-white gap-8 uppercase text-sm text-white font-medium relative after:absolute after:bottom-0 after:left-0 after:bg-teal-400 after:h-0.5 after:w-0 hover:after:w-full after:transition-all after:duration-500 after:ease-in-out mx-2'>
          <Link to='/series'>Series</Link>
        </li>
      </ul>
      <div onClick={handleNav} className='block md:hidden'>
          {nav ? <AiOutlineClose size={20}/> : <AiOutlineMenu size={20} />}
      </div>
      <ul className={nav ? 'fixed left-0 top-0 w-[60%] h-full border-r border-r-gray-900 bg-[#000300] ease-in-out duration-500' : 'ease-in-out duration-500 fixed left-[-100%]'}>
        <h1 className='w-full text-3xl font-bold text-[#00df9a] m-4'>Movies.</h1> 
          <li className='p-4 border-b border-gray-600'>Home</li>
          <li className='p-4 border-b border-gray-600'>Movies</li>
          <li className='p-4 border-b border-gray-600'>Series</li>
          <li className='p-4 border-b border-gray-600'>Login</li>
      </ul>
    </div>
  );
};

export default Navbar;
