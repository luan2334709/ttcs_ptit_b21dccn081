const Series = require("../../models/Series.model");
const Fuse = require("fuse.js");

//* [GET] /serries/
module.exports.index = async (req, res) => {
    try {
        const films = await Series.find({}).limit(60);
        res.json(films);
    } catch (error) {
        console.error("Error in index module: ", error);
        next(error);
    }
};

//* [GET] /series/action
module.exports.action = async (req, res) => {
    try {
        const actionId = "660e346c39ba78d97a6f8a10";
        const series = await Series.find({
            categories: { $in: [actionId] },
        }).limit(60);
        res.json(series);
    } catch (error) {
        console.log("Error in action module: ", error);
        next(error);
    }
};

//* [GET] /series/romantic
module.exports.romantic = async (req, res) => {
    try {
        const romanticID = "660e353739ba78d97a70a756";
        const series = await Series.find({
            categories: { $in: [romanticID] },
        }).limit(60);
        res.json(series);
    } catch (error) {
        console.log("Error in action module: ", error);
        next(error);
    }
};

//* [GET] /series/hornor
module.exports.hornor = async (req, res, next) => {
    try {
        const hornorID = "660e360739ba78d97a71dc68";
        const series = await Series.find({
            categories: { $in: [hornorID] },
        }).limit(60);
        res.json(series);
    } catch (error) {
        console.log("Error in action module: ", error);
        next(error);
    }
};

//* [GET] /series/cartoon
module.exports.cartoon = async (req, res) => {
    try {
        const cartoonID = "660e376f39ba78d97a73db18";
        const series = await Series.find({
            categories: { $in: [cartoonID] },
        }).limit(60);
        res.json(series);
    } catch (error) {
        console.log("Error in action module: ", error);
        next(error);
    }
};

//* [GET] /serries/details/:id
module.exports.detail = async (req, res, next) => {
    try {
        const film = await Series.findById(req.params.id);
        if (!film) {
            return res.status(404).send("Film not found");
        }
        res.json(film);
    } catch (error) {
        console.error("Error in detail module: ", error);
        next(error);
    }
};

//* [POST] /serries/search
module.exports.search = async (req, res) => {
    console.log(req.body);
    const options = {
        keys: ["title", "engName"],
    };
    let series = await Series.find({});
    const fuse = new Fuse(series, options);
    if (req.body.term) {
        const results = fuse.search(req.body.term);
        const uniqueSeries = new Map();
        results.forEach((result) => {
            const series = result.item;
            if (!uniqueSeries.has(series.title)) {
                uniqueSeries.set(series.title, series);
            }
        });
        series = Array.from(uniqueSeries.values());
    }
    res.json(series);
};

//* [GET] /series/details/:id/:part
module.exports.part = async (req, res, next) => {
    try {
        const film = await Series.findById(req.params.id);
        const part = req.params.part;
        console.log(req.params);
        if (!film) {
            return res.status(404).send("Film not found");
        }
        if (part > film.video_links.length) {
            return res.status(404).send("Part not found");
        }
        res.json(film.video_links[part - 1]);
    } catch (error) {
        console.error("Error in part module: ", error);
        next(error);
    }
};
