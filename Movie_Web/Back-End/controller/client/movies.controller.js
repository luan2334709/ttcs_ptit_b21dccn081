const Film = require("../../models/Film.model.js");
const Fuse = require("fuse.js");

//* [GET] /movies
module.exports.index = async (req, res) => {
    try {
        const movies = await Film.find({}).limit(60);
        res.json(movies);
    } catch (error) {
        console.log("Error in index module: ", error);
        next(error);
    }
};

//* [GET] /movies/action
module.exports.action = async (req, res) => {
    try {
        const actionId = "660e346c39ba78d97a6f8a10";
        const movies = await Film.find({ Sgeneros: { $in: [actionId] } }).limit(
            60
        );
        res.json(movies);
    } catch (error) {
        console.log("Error in action module: ", error);
        next(error);
    }
};

//* [GET] /movies/romantic
module.exports.romantic = async (req, res) => {
    try {
        const romanticID = "660e353739ba78d97a70a756";
        const movies = await Film.find({
            Sgeneros: { $in: [romanticID] },
        }).limit(60);
        res.json(movies);
    } catch (error) {
        console.log("Error in action module: ", error);
        next(error);
    }
};

//* [GET] /movies/hornor
module.exports.hornor = async (req, res, next) => {
    try {
        const hornorID = "660e360739ba78d97a71dc68";
        const movies = await Film.find({
            Sgeneros: { $in: [hornorID] },
        }).limit(60);
        res.json(movies);
    } catch (error) {
        console.log("Error in action module: ", error);
        next(error);
    }
};
//* [GET] /movies/cartoon
module.exports.cartoon = async (req, res) => {
    try {
        const cartoonID = "660e376f39ba78d97a73db18";
        const movies = await Film.find({
            Sgeneros: { $in: [cartoonID] },
        }).limit(60);
        res.json(movies);
    } catch (error) {
        console.log("Error in action module: ", error);
        next(error);
    }
};
//* [GET] /movies/details/:id
module.exports.detail = async (req, res) => {
    const id = req.params.id;
    try {
        const movie = await Film.findById(id);
        if (!movie) {
            return res.status(404).send("Movie not found");
        }
        res.json(movie);
    } catch (error) {
        console.log("Error in detail module: ", error);
        next(error);
    }
};

module.exports.search = async (req, res) => {
    console.log(req.body);
    const options = {
        keys: ["Name", "engName"],
    };
    movies = await Film.find({});
    const fuse = new Fuse(movies, options);
    if (req.body.term) {
        const results = fuse.search(req.body.term);
        const uniqueMovies = new Map();
        results.forEach((result) => {
            const movie = result.item;
            if (!uniqueMovies.has(movie.Name)) {
                uniqueMovies.set(movie.Name, movie);
            }
        });
        movies = Array.from(uniqueMovies.values());
    }
    res.json(movies);
};
