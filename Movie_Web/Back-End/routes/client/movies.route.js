const router = require("express").Router();
const moviesController = require("../../controller/client/movies.controller");

router.get("/", moviesController.index);
router.get("/action", moviesController.action);
router.get("/romantic", moviesController.romantic);
router.get("/hornor", moviesController.hornor);
router.get("/cartoon", moviesController.cartoon);
router.post("/search", moviesController.search);
router.get("/detail/:id", moviesController.detail);

module.exports = router;
