const SeriesRoutes = require("./series.route");
const MoviesRoutes = require("./movies.route");

module.exports = (app) => {
    app.use("/series", SeriesRoutes);
    app.use("/movies", MoviesRoutes);
};
