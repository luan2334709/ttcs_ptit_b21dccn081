const router = require("express").Router();
const seriesController = require("../../controller/client/series.controller");

router.get("/", seriesController.index);
router.get("/detail/:id", seriesController.detail);
router.get("/detail/:id/:part", seriesController.part);
router.get("/action", seriesController.action);
router.get("/romantic", seriesController.romantic);
router.get("/hornor", seriesController.hornor);
router.get("/cartoon", seriesController.cartoon);
router.post("/search", seriesController.search);

module.exports = router;
