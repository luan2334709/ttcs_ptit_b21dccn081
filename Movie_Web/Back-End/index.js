require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const cors = require("cors");
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
const Route = require("./routes/client/index.route");
const port = process.env.PORT;
const connect = require("./config/database.js");
connect.connection();
Route(app);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
